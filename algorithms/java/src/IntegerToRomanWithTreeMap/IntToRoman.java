// Author : Emilio de Oliveira
// Date   : 2023-12-07
//
//
// Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
//
//        Symbol       Value
//        I             1
//        V             5
//        X             10
//        L             50
//        C             100
//        D             500
//        M             1000
//        For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
//
//        Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
//
//        I can be placed before V (5) and X (10) to make 4 and 9.
//        X can be placed before L (50) and C (100) to make 40 and 90.
//        C can be placed before D (500) and M (1000) to make 400 and 900.
//        Given an integer, convert it to a roman numeral.
//
//
//
//        Example 1:
//
//        Input: num = 3
//        Output: "III"
//        Explanation: 3 is represented as 3 ones.
//        Example 2:
//
//        Input: num = 58
//        Output: "LVIII"
//        Explanation: L = 50, V = 5, III = 3.
//        Example 3:
//
//        Input: num = 1994
//        Output: "MCMXCIV"
//        Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.

package algorithms.java.src.IntegerToRomanWithTreeMap;

import java.util.TreeMap;

public class IntToRoman {
    public String intToRomanConverter(int num) {
        StringBuilder romanNumeral = new StringBuilder();
        TreeMap<Integer, String> charMap = new TreeMap<>();
        charMap.put(1000, "M");
        charMap.put(900, "CM");
        charMap.put(500, "D");
        charMap.put(400, "CD");
        charMap.put(100, "C");
        charMap.put(90, "XC");
        charMap.put(50, "L");
        charMap.put(40, "XL");
        charMap.put(10, "X");
        charMap.put(9, "IX");
        charMap.put(5, "V");
        charMap.put(4, "IV");
        charMap.put(1, "I");

        int multiplier = 0;
        do {
            int floorKey = charMap.floorKey(num);
            multiplier = num / floorKey;
            for (int i = 0; i <= multiplier -1; i++) {
                romanNumeral.append(charMap.get(floorKey));
                if (num - floorKey == 0 && num != 1) {
                    String x_str = Integer.toString(num);
                    num = ConvertIntoInteger(x_str.substring(1));
                } else if (num == 1) {
                    num = 0;
                } else {
                    num = num - floorKey;
                }
            }
        } while (num > 0);
        return romanNumeral.toString();
    }
    private int ConvertIntoInteger(String val){
        try
        {
            return Integer.parseInt(val);
        }
        catch(Exception ex)
        {
            return 0;
        }
    }

}

