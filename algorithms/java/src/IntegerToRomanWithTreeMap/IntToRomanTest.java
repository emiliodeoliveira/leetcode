package algorithms.java.src.IntegerToRomanWithTreeMap;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class IntToRomanTest {
    @Test
    public void testIntToRomanConverter() throws Exception {
        IntToRoman intToRoman = new IntToRoman();
        int number = 3724;
        String expectedResult = "MMMDCCXXIV";
        String result = intToRoman.intToRomanConverter(number);
        assertEquals(result, expectedResult);
    }
}