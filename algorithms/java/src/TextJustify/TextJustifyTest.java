package algorithms.java.src.TextJustify;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextJustifyTest {
    @Test
    public void testFullJustify() throws Exception {
        TextJustify textJustify = new TextJustify();
        String[] words = {"What","must","be","acknowledgment","shall","be"};
        List<String> actualResult =  textJustify.fullJustify(words,16);
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("What   must   be");
        expectedResult.add("acknowledgment  ");
        expectedResult.add("shall be        ");
        assertEquals(expectedResult,actualResult);
    }
}