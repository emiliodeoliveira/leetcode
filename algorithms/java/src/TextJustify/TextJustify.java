// Author : Emilio de Oliveira
// Date   : 2023-12-07
//
//Given an array of strings words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.
//
//        You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.
//
//        Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line does not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.
//
//        For the last line of text, it should be left-justified, and no extra space is inserted between words.
//
//        Note:
//
//        A word is defined as a character sequence consisting of non-space characters only.
//        Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
//        The input array words contains at least one word.
//
//
//        Example 1:
//
//        Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
//        Output:
//        [
//        "This    is    an",
//        "example  of text",
//        "justification.  "
//        ]
//        Example 2:
//
//        Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
//        Output:
//        [
//        "What   must   be",
//        "acknowledgment  ",
//        "shall be        "
//        ]
//        Explanation: Note that the last line is "shall be    " instead of "shall     be", because the last line must be left-justified instead of fully-justified.
//        Note that the second line is also left-justified because it contains only one word.
//        Example 3:
//
//        Input: words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], maxWidth = 20
//        Output:
//        [
//        "Science  is  what we",
//        "understand      well",
//        "enough to explain to",
//        "a  computer.  Art is",
//        "everything  else  we",
//        "do                  "
//        ]
//
//
//        Constraints:
//
//        1 <= words.length <= 300
//        1 <= words[i].length <= 20
//        words[i] consists of only English letters and symbols.
//        1 <= maxWidth <= 100
//        words[i].length <= maxWidth


package algorithms.java.src.TextJustify;

import java.util.ArrayList;
import java.util.List;

public class TextJustify {
    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> text = new ArrayList<>();
        int listSize = words.length;
        int left = 0;
        while (left < listSize) {
            int right = findRightValues(left, words, maxWidth);
            text.add(justifyText(left, right, words, maxWidth));
            left = right + 1;
        }
        return text;
    }

    private static String justifyText(int left, int right, String[] words, int maxWidth) {
        StringBuilder result = new StringBuilder();
        boolean isLastLine = right == words.length - 1;
        int numSpaces = right - left;
        int totalSpace = calculateSpace(maxWidth, left, right, words);
        String space = isLastLine ? " " : blankSpace(totalSpace, numSpaces);
        int remaining = isLastLine ? 0 : calRemainingSpace(totalSpace,numSpaces);
        for (int i = left; i <= right; i++)
            result.append(words[i])
                    .append(space)
                    .append(remaining-- > 0 ? " " : "");
        return padding(result.toString().trim(), maxWidth);
    }

    private static int calRemainingSpace(int totalSpace, int numSpaces) {
        if (totalSpace > 0 && numSpaces > 0) {
            return totalSpace % numSpaces;
        } else {
            return 0;
        }
    }

    private static int calculateSpace(int maxWidth, int left, int right, String[] words) {
        int wordsLength = 0;
        for (int i = left; i <= right; i++) wordsLength += words[i].length();
        return maxWidth - wordsLength;
    }

    private static int findRightValues(int left, String[] words, int maxWidth) {
        int right = left;
        int sum = words[right++].length();

        while (right < words.length && (sum + 1 + words[right].length()) <= maxWidth)
            sum += 1 + words[right++].length();

        return right - 1;
    }

    private static String padding(String result, int maxWidth) {
        return result + addBlankSpace(maxWidth - result.length());
    }

    private static String addBlankSpace(int i) {
        return new String(new char[i]).replace('\0', ' ');
    }

    private static String blankSpace(int totalSpace, int numSpaces) {
        String result = null;
        if (totalSpace > 0 && numSpaces > 0) {
            return new String(new char[totalSpace / numSpaces]).replace('\0', ' ');
        } else {
            return new String(new char[0]).replace('\0', ' ');
        }
    }
}