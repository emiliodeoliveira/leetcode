package algorithms.java.src.RomanToIntegerWithHashMap;

import algorithms.java.src.IntegerToRomanWithTreeMap.IntToRoman;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RomanToIntTest {
    @Test
    public void testRomanToIntConverter() throws Exception {
        RomanToInt romanToInt = new RomanToInt();
        int expectedResult = 3724;
        String romanNumeral = "MMMDCCXXIV";
        int result = romanToInt.romanToIntConverter(romanNumeral);
        assertEquals(result, expectedResult);
    }

}